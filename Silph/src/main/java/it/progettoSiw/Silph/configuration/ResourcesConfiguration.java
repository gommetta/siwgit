package it.progettoSiw.Silph.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class ResourcesConfiguration extends WebMvcConfigurerAdapter {
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
        			"/assets/**",
                "/fonts/**",
                "/css/**",
                "/js/**",
                "/fotografiPage/**",
                "/images/**"
        			)
                .addResourceLocations(
                		"classpath:/static/assets/",
                        "classpath:/static/fonts/",
                        "classpath:/static/css/",
                        "classpath:/static/css/",
                        "classpath:/static/js/",
                        "classpath:/static/fotografiPage/",
                        "classpath:/static/images/"
                			);
        
        registry.addResourceHandler("/photographersimgs/**").addResourceLocations("/photographersimgs/","file:photographersimgs/");
        registry.addResourceHandler("/photos/**").addResourceLocations("/photos/","file:photos/");
    }

}
