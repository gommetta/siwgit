package it.progettoSiw.Silph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SilphApplication {

	public static void main(String[] args) {
		SpringApplication.run(SilphApplication.class, args);
	}

}
