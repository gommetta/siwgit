package it.progettoSiw.Silph.repository;

import org.springframework.data.repository.CrudRepository;

import it.progettoSiw.Silph.model.ShoppingCart;

public interface ShoppingCartRepository extends CrudRepository<ShoppingCart, Long>{

}
