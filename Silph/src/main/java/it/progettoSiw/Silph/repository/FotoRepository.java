package it.progettoSiw.Silph.repository;

import org.springframework.data.repository.CrudRepository;

import it.progettoSiw.Silph.model.Foto;

public interface FotoRepository extends CrudRepository<Foto, Long> {

}
