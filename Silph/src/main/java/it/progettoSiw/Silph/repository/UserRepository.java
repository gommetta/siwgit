package it.progettoSiw.Silph.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.progettoSiw.Silph.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}