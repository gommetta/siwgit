package it.progettoSiw.Silph.repository;

import org.springframework.data.repository.CrudRepository;

import it.progettoSiw.Silph.model.Photographer;

public interface PhotographerRepository extends CrudRepository<Photographer, Long> {

}
