package it.progettoSiw.Silph.repository;

import org.springframework.data.repository.CrudRepository;

import it.progettoSiw.Silph.model.Album;

public interface AlbumRepository extends CrudRepository<Album, Long> {

}
