package it.progettoSiw.Silph.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.service.AlbumService;
import it.progettoSiw.Silph.service.PhotographerService;

@Controller
public class PhotographerController {
	
	@Autowired
	private PhotographerService photographerService;
	@Autowired
	private AlbumService aService;
	
	@RequestMapping(value="/photographer/{id}", method = RequestMethod.GET)
	public String ArtistaEAlbums(@PathVariable("id") String id,Model m) {
		long iddo = Long.parseLong(id);
		Photographer pippo= photographerService.getPhotographerById(iddo);
		m.addAttribute("photographer", pippo);
		m.addAttribute("photograpersAlbums",pippo.getAlbums());
		return "Artista&Albums";
	}

}
