package it.progettoSiw.Silph.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.progettoSiw.Silph.model.Album;
import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.service.AlbumService;
import it.progettoSiw.Silph.service.PhotographerService;

@Controller
public class AlbumController {
	
	@Autowired
	private AlbumService albumService;

	@RequestMapping(value="/album/{id}", method = RequestMethod.GET)
	public String album(@PathVariable("id") String id, Model m) {
		long iddo = Long.parseLong(id);
		Album albi= albumService.getAlbumById(iddo);
		m.addAttribute("album", albi);
		m.addAttribute("albulmsFotos", albi.getFotos());

		return "Album";
	}

}
