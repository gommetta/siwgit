package it.progettoSiw.Silph.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.repository.FotoRepository;
import it.progettoSiw.Silph.service.AlbumService;
import it.progettoSiw.Silph.service.FotoService;
import it.progettoSiw.Silph.service.PhotographerService;

@Controller
public class FotoController {
		
	@Autowired
	private FotoService fotoService;
	
	@RequestMapping(value="/gallery", method = RequestMethod.GET)
	public String Gallery(Model m) {
		m.addAttribute("fotos", fotoService.allFotosSorted());
		return "Gallery";
	}

}
