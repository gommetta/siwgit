package it.progettoSiw.Silph.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import it.progettoSiw.Silph.model.Album;
import it.progettoSiw.Silph.model.Foto;
import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.service.AlbumService;
import it.progettoSiw.Silph.service.FotoService;
import it.progettoSiw.Silph.service.PhotographerService;
import it.progettoSiw.Silph.service.UploadImagesService;

@Controller
public class TestAdminController {
	
	@Autowired
	private PhotographerService phService;
	
	@Autowired
	private AlbumService albumService;
	
	@Autowired
	private UploadImagesService uploadService;
	
	@Autowired
	private FotoService fotoService;
	

	@RequestMapping(value = { "/adminhome" }, method = RequestMethod.GET)
    public String welcome(Model model) {
        UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String role = details.getAuthorities().iterator().next().getAuthority();     // get first authority
        model.addAttribute("username", details.getUsername());
        model.addAttribute("role", role);

        return "admin";
    }

    /**
     * This method is called when a GET request is sent by the user to URL "/admin".
     * This method prepares and dispatches the admin view.
     *
     * @return the name of the admin view
     */
    @RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
    public String admin(Model model) {
        UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String role = details.getAuthorities().iterator().next().getAuthority();    // get first authority
        model.addAttribute("username", details.getUsername());
        model.addAttribute("role", role);

        return "admin";
    }
    
    @RequestMapping(value = "/admin/nuovofotografo")
    public String nuovoFotografo() {
    	return "AggiungiFotografo";
    }
    
    @RequestMapping(value = "/admin/nuovafoto")
    public String nuovaFoto() {
    	return "AggiungiFoto";
    }
    
    //AGGIUNGI FOTO
    @RequestMapping(value = "/admin/aggiungifoto")
    public String aggiungiFoto(Model m) {
    	//questo metodo gestisce l'inizio del flusso di nuova foto in admin
    	//prima selezione fotografo, poi selezione album e infine form
    	
    	return "redirect:/admin/aggiungifoto/listaphotographer";
    }
    
    @RequestMapping(value = "/admin/aggiungifoto/listaphotographer")
    public String listaPhotographerPerFoto(Model m) {
    	m.addAttribute("photographers", phService.allPhotographers());
    	m.addAttribute("modalita","aggiungifoto");
    	m.addAttribute("nextpage", "listaalbum");
    	return "AdminListaFotografi";
    }
    
    @RequestMapping(value = "/admin/aggiungifoto/listaalbum")
    public String listaAlbums(Model m, @RequestParam(required = true, value = "idPhotographer") String idPhotographer ) {
    	Long iddo = Long.parseLong(idPhotographer);
    	Photographer ph = phService.getPhotographerById(iddo);
    	m.addAttribute("photographer", ph);
    	//DA IMPLEMENTARE
    	m.addAttribute("albums", ph.getAlbums());
    	System.out.println("\n\nmostrando album di "+ idPhotographer+ " "+ph.getAlbums().size() +"\n\n");
    	return "AdminListaAlbum";
    }
    
    @RequestMapping(value = "/admin/aggiungifoto/formcreazione")
    public String nuovaFoto(Model m,@RequestParam(required = true, value = "idPhotographer") String idPhotographer, @RequestParam(required = true, value = "idAlbum") String idAlbum) {
    	Long idPh = Long.parseLong(idPhotographer);
    	Long idAlb = Long.parseLong(idAlbum);
    	
    	Photographer ph = phService.getPhotographerById(idPh);
    	Album album = albumService.getAlbumById(idAlb);
    	m.addAttribute("photographer", ph);
    	m.addAttribute("album", album);
    	//aggiunge la nuova foto i cui campi verranno popolati
    	m.addAttribute("foto", new Foto());
    	//VA AL FORM
    	
    	return "AggiungiFoto";
    }
    
    @RequestMapping(value = "/admin/aggiungifoto/confermacreazione/{idPhotographer}/{idAlbum}")
    public String nuovaFotoSubmit(@Valid @ModelAttribute Foto foto,Model m,@PathVariable(name="idPhotographer") String idPhotographer, @PathVariable(name = "idAlbum") String idAlbum, @RequestParam("file") MultipartFile file ) {
    	Long idPh = Long.parseLong(idPhotographer);
    	Long idAlb = Long.parseLong(idAlbum);
    	
    	Photographer ph = phService.getPhotographerById(idPh);
    	Album album = albumService.getAlbumById(idAlb);
    	
    	foto.setAlbum(album);
    	//SALVA FOTO SU SERVICE
    	foto.setPath("");
    	fotoService.save(foto);
    	
    	String imagePath = uploadService.saveNewPhoto(ph, album, foto, file);
    	foto.setPath(imagePath);
    	
    	//RISALVA FOTO SU SERVICE
    	fotoService.save(foto);
    	
    	System.out.println("\n\nnuova foto aggiunta "+ imagePath+"\n\n");
    	

    	return "redirect:/admin";
    }
    
    //AGGIUNGI ALBUM
    
    @RequestMapping(value = "/admin/aggiungialbum")
    public String aggiungiAlbum(Model m) {
    	//questo metodo gestisce l'inizio del flusso di un nuovo album in admin
    	//prima selezione fotografo, poi form
    	
    	return "redirect:/admin/aggiungialbum/listaphotographer";
    	
    }
    
    @RequestMapping(value = "/admin/aggiungialbum/listaphotographer")
    public String listaPhotographerPerAlbum(Model m) {
    	m.addAttribute("photographers", phService.allPhotographers());
    	m.addAttribute("modalita","aggiungialbum");
    	m.addAttribute("nextpage", "formcreazione");
    	return "AdminListaFotografi";
    }
    
    @RequestMapping(value = "/admin/aggiungialbum/formcreazione")
    public String nuovoAlbum(Model m, @RequestParam(required = true, value = "idPhotographer") String idPhotographer) {
    	Long idPh = Long.parseLong(idPhotographer);
    	Photographer ph = phService.getPhotographerById(idPh);
    	m.addAttribute("photographer", ph);
    	m.addAttribute("album", new Album());
    	return "AggiungiAlbum";
    }
    
    @RequestMapping(value = "/admin/aggiungialbum/confermacreazione/{idPhotographer}")
    public String nuovoAlbumSubmit(@Valid @ModelAttribute Album album,Model m, @PathVariable(name = "idPhotographer") String idPhotographer) {
    	Long idPh = Long.parseLong(idPhotographer);
    	
    	Photographer ph = phService.getPhotographerById(idPh);
    	album.setPhotographer(ph);
    	
    	albumService.save(album);
    	
    	
    	return "redirect:/admin";
    }
    
    //AGGIUNGI FOTOGRAFO
    
    @RequestMapping(value = "/admin/aggiungiphotographer")
    public String aggiungiPhotographer(Model m) {
    	//questo metodo gestisce l'inizio del flusso di un nuovo album in admin
    	//prima selezione fotografo, poi form
    	
    	return "redirect:/admin/aggiungiphotographer/formcreazione";
    	
    }
    
    
    @RequestMapping(value = "/admin/aggiungiphotographer/formcreazione")
    public String nuovoPhotographer(Model m) {
    	m.addAttribute("photographer", new Photographer());
    	return "AggiungiFotografo";
    }
    
    @RequestMapping(value = "/admin/aggiungiphotographer/confermacreazione")
    public String nuovoPhotographerSubmit(@Valid @ModelAttribute Photographer photographer,Model m, @RequestParam("file") MultipartFile file) {
    	
    	photographer.setAvatarPath("");
    	phService.save(photographer);
    	
    	String path = uploadService.saveAvatar(photographer, file);
    	
    	photographer.setAvatarPath(path);
    	phService.save(photographer);
    	
    	
    	return "redirect:/admin";
    }

	
}
