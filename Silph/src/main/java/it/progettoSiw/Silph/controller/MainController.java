package it.progettoSiw.Silph.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.progettoSiw.Silph.model.Foto;
import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.model.ShoppingCart;
import it.progettoSiw.Silph.repository.PhotographerRepository;
import it.progettoSiw.Silph.repository.ShoppingCartRepository;
import it.progettoSiw.Silph.service.FotoService;
import it.progettoSiw.Silph.service.PhotographerService;
import it.progettoSiw.Silph.service.ShoppingCartService;

@Controller
public class MainController {
	@Autowired
	private PhotographerService photographerService;
	
	@Autowired
	private FotoService fotoService;
	
	@Autowired
	private ShoppingCartService cartService;

    @RequestMapping(value="/", method= RequestMethod.GET)
    public String home(Model m) {
        m.addAttribute("photographers", photographerService.allPhotographers());
		return "Home";
	}
    
    @RequestMapping(value="/generic", method= RequestMethod.GET)
    public String generic() {
    	return "generic";
    }
    
    @RequestMapping(value = "/addtocart")
    public String aggiungiAlCarrello(HttpSession httpSession,Model m, @RequestParam(required = true, value = "idPhoto") String idPhoto) {
    	Long idFoto = Long.parseLong(idPhoto);
    	Foto fotoDaAggiungere = fotoService.getFotoById(idFoto);
    	
    	if (httpSession.getAttribute("cart") == null){
    		ShoppingCart cart = new ShoppingCart();
    		
    		cart.addFotoToCart(fotoDaAggiungere);
    		httpSession.setAttribute("cart",cart);
    		
    	}else {
    		ShoppingCart cart = (ShoppingCart)httpSession.getAttribute("cart");
    		cart.addFotoToCart(fotoDaAggiungere);
    		httpSession.setAttribute("cart",cart);
    	}
    	
    	return "redirect:/cart";
    }
    
    @RequestMapping(value="/cart")
    public String cart(HttpSession httpSession, Model m) {
    	ShoppingCart cart = (ShoppingCart)httpSession.getAttribute("cart");
    	if (cart == null){
    		
    		
    	}else {
    		m.addAttribute("cart", cart);
    	}
    	
    	return "formUser";
    }
    
    @RequestMapping("/cart/annulla")
    public String deleteCart(HttpSession httpSession) {
    	httpSession.setAttribute("cart",null);
    	return "redirect:/";
    }
    
    @RequestMapping("/cart/conferma")
    public String confermaCart(@Valid @ModelAttribute ShoppingCart cart, Model m) {
    	cartService.save(cart);
    	
    	return "redirect:/";
    }

}
    
