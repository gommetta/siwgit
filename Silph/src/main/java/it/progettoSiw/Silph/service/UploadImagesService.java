package it.progettoSiw.Silph.service;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import it.progettoSiw.Silph.model.Album;
import it.progettoSiw.Silph.model.Foto;
import it.progettoSiw.Silph.model.Photographer;

@Service
public class UploadImagesService {
	
	public static String photosDirectory = new File(System.getProperty("user.dir") +File.separator+"photos/").getPath();
	public static String photographerImageDirectory = new File(System.getProperty("user.dir") +File.separator+"photographersimgs/").getPath();

	public UploadImagesService() {
		super();
		new File(photosDirectory).mkdir();
		new File(photographerImageDirectory).mkdir();
	}
	private boolean upload(MultipartFile file, String name,String uploadDir) {
		
		try {
			File transferFile = new File(uploadDir+ "/" + name); 
			file.transferTo(transferFile);
			return true;
		}catch(Exception e) {
			System.out.println("\n\nerrore upload file \n\n");
		}
		
		return false;
		
	}
	
	public String saveNewPhoto(Photographer ph, Album album, Foto foto, MultipartFile file) {
		String cognome = ph.getCognome() == null ? "" : ph.getCognome();
		String filename = "" + cognome + ph.getId().toString() + "_" + album.getId().toString() + "_" + foto.getId().toString() + "."+ FilenameUtils.getExtension(file.getOriginalFilename());
		
		boolean result = this.upload(file, filename, photosDirectory);
		if (result) 
			return "/photos/" +filename;
		else
			return null;
	}
	
	public String saveAvatar(Photographer ph, MultipartFile file) {
		String filename = "" + ph.getCognome() + ph.getId().toString()+ "."+ FilenameUtils.getExtension(file.getOriginalFilename());
		
		boolean result = this.upload(file, filename, photographerImageDirectory);
		if (result) 
			return "/photographersimgs/" + filename;
		else
			return null;
	}
	
	public String getFinalName(String filename) {
		return "/uploads/" + filename;
	}
}
