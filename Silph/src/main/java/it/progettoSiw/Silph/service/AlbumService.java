package it.progettoSiw.Silph.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.progettoSiw.Silph.model.Album;
import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.repository.AlbumRepository;
import it.progettoSiw.Silph.repository.PhotographerRepository;

@Service
public class AlbumService {
	
	@Autowired
	private AlbumRepository albumRepository;
	@Autowired
	private PhotographerService pService;


	public Album getAlbumById(long id) {
		return this.albumRepository.findById(id).orElse(null);
	}
	
	public List<Album> allAlbums(){
		ArrayList<Album> al = new ArrayList<Album>();
		this.albumRepository.findAll().iterator().forEachRemaining(al::add);
		return al;
	}
	
	public List<Album> allAlbumsByPhotographer(Photographer p){
		ArrayList<Album> al = new ArrayList<Album>();
		for (Album album : this.albumRepository.findAll()) {
			if (album.getPhotographer().equals(p)) al.add(album);	
		}	
		return al;
	}

	@Transactional
	public void saveTest() {
		Photographer gino = pService.allPhotographers().get(0);
		
		Album a = new Album("a",gino,0);
		Album b = new Album("b", gino,0);
		Album c = new Album ("c", gino,0);	
		albumRepository.save(a);
		albumRepository.save(b);
		albumRepository.save(c);
		
	}
	@Transactional
	public void save(Album album) {
		albumRepository.save(album);
		
	}

}
