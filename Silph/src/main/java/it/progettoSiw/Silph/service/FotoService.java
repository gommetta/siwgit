package it.progettoSiw.Silph.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.progettoSiw.Silph.model.Album;
import it.progettoSiw.Silph.model.Foto;
import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.repository.AlbumRepository;
import it.progettoSiw.Silph.repository.FotoRepository;

@Service
public class FotoService {
	@Autowired
	private AlbumService aService;
	@Autowired
	private PhotographerService pService;
	@Autowired
	private FotoRepository fotoRepository;
	
	public Foto getFotoById(long id) {
		return this.fotoRepository.findById(id).orElse(null);
	}

	public List<Foto> allFotos(){
		ArrayList<Foto> fo = new ArrayList<Foto>();
		this.fotoRepository.findAll().iterator().forEachRemaining(fo::add);
		return fo;
	}
	public List<Foto> allFotosSorted(){
		ArrayList<Foto> fo = new ArrayList<Foto>();
		this.fotoRepository.findAll().iterator().forEachRemaining(fo::add);
		Collections.sort(fo);
		return fo;
	}

	
	public List<Foto> allFotosByAlbum(Album a){
		ArrayList<Foto> fo = new ArrayList<Foto>();
		for (Foto foto : this.fotoRepository.findAll()) {
			if (foto.getAlbum().equals(a)) fo.add(foto);	
		}	
		return fo;
	}
	
	@Transactional
	public void saveTest() {
		Photographer gino = pService.allPhotographers().get(0);
		Album al= aService.allAlbums().get(0);
		Album ab= aService.allAlbums().get(1);
		
		Foto a = new Foto("FotoA","/images/fulls/06.jpg",0,al);
		Foto b = new Foto("FotoB", "/images/fulls/07.jpg",0,al);
		Foto c = new Foto ("FotoC", "/images/fulls/05.jpg",0,ab);	
		fotoRepository.save(a);
		fotoRepository.save(b);
		fotoRepository.save(c);
		
	}

	@Transactional
	public void save(Foto foto) {
		fotoRepository.save(foto);
		
	}
}
