package it.progettoSiw.Silph.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.model.ShoppingCart;
import it.progettoSiw.Silph.repository.ShoppingCartRepository;

@Service
public class ShoppingCartService {
	
	@Autowired
	private ShoppingCartRepository sRepository;
	
	
	@Transactional
	public void save(ShoppingCart cart) {
		sRepository.save(cart);
	}
	
	public List<ShoppingCart> getAll(){
		ArrayList<ShoppingCart> ph = new ArrayList<ShoppingCart>();
		this.sRepository.findAll().iterator().forEachRemaining(ph::add);
		return ph;
	}

}
