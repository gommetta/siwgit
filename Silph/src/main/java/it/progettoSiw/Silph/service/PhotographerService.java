package it.progettoSiw.Silph.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.progettoSiw.Silph.model.Album;
import it.progettoSiw.Silph.model.Photographer;
import it.progettoSiw.Silph.repository.PhotographerRepository;

@Service
public class PhotographerService {
	
	@Autowired
	private PhotographerRepository photographerRepository;

	public Photographer getPhotographerById(Long id) {
		return this.photographerRepository.findById(id).orElse(null);
	}
	public List<Photographer> allPhotographers(){
		ArrayList<Photographer> ph = new ArrayList<Photographer>();
		this.photographerRepository.findAll().iterator().forEachRemaining(ph::add);
		return ph;
		
	}
	
	@Transactional
	public void saveTest() {
		Photographer gino = new Photographer("gino", "ciccio","https://www.dandi.media/wp-content/uploads/2016/10/claudio-santamaria.jpg");
		Photographer giorgio = new Photographer("giorgio", "ciccio","http://www.voceapuana.com/foto/2018/07/30/rohafyij.jpg");
		Photographer geppa = new Photographer("geppa", "la","http://www.luciaboranga.it/wp-content/uploads/2018/05/Lucia-Boranga-coach-life-e-business-Montebelluna-Treviso.jpg");	
		photographerRepository.save(gino);
		photographerRepository.save(giorgio);
		photographerRepository.save(geppa);
		
	}
	@Transactional
	public void save(Photographer p) {
		photographerRepository.save(p);
		
	}
}
