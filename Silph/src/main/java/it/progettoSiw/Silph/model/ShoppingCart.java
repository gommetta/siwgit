package it.progettoSiw.Silph.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ShoppingCart {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column
	private String name;

	@Column
	private String cognome;
	@Column (length= 2000)
	private String descrizione;
	@Column
	private String email;
	@OneToMany
	private List<Foto> selezionate;
	
	public void addFotoToCart(Foto a) {
		this.selezionate.add(a);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Foto> getSelezionate() {
		return selezionate;
	}
	public void setSelezionate(List<Foto> selezionate) {
		this.selezionate = selezionate;
	}
	public Long getId() {
		return id;
	}
	
	
}
