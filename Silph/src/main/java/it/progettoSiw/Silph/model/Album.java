package it.progettoSiw.Silph.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(	name="albums",
		uniqueConstraints=@UniqueConstraint(columnNames={"photographer_id", "name"}))
@NamedQuery(name= "findAllAlbums", query= "SELECT a FROM Album a")
public class Album implements Comparable<Album>{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(nullable = false)
	private String name;
	
	@ManyToOne
	private Photographer photographer;
	
	@OneToMany (mappedBy = "album")
	private List <Foto> fotos;
	@Column 
	private int click;
	
//COSTRUTTORI
	public Album(String name, Photographer photographer, int click) {
		super();
		this.name = name;
		this.photographer = photographer;
		this.click = click;
	}
	public Album() {
		this.click = 0;
	}
	
//METODI
	public List<Foto> getOrderedByClickFotos() {
		return sortByClick(fotos);
	}
	
	public List<Foto> sortByClick(List<Foto> fotos){
		List <Foto> fotosOrdinate  = new ArrayList<Foto> (fotos);
		Collections.sort(fotosOrdinate);
		return fotosOrdinate;
	}
	public String getFotoCliccata() {
		List <Foto> fotosOrdinate  = new ArrayList<Foto> (fotos);
		Collections.sort(fotosOrdinate);
		if(fotosOrdinate.size() == 0) return "/images/icons/album.svg"; //forse non si dovrebbe vedere l'album
		if(fotosOrdinate.get(0).getPath()== null) return "/images/icons/album.svg";
		return fotosOrdinate.get(0).getPath();
	}

	//GET & SET
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Photographer getPhotographer() {
		return photographer;
	}

	public void setPhotographer(Photographer photographer) {
		this.photographer = photographer;
	}
	public List<Foto> getFotos() {
		return fotos;
	}
	public int getClick() {
		return click;
	}

	public void setClick(int click) {
		this.click = click;
	}

	public void setFotos(List<Foto> fotos) {
		this.fotos = fotos;
	}

	
	
	//CompareTo in ordine di click e in caso di parità in ordine alfabetico (gli album dello stesso autore non possono avere nome uguale)
		@Override
		public int compareTo(Album a) {
			if (this.click==a.getClick()) return this.name.compareTo(a.getName());
			else return this.click - a.getClick();
		}
}