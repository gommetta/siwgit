package it.progettoSiw.Silph.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table (name="fotos")
@NamedQuery(name= "findAllFotos", query= "SELECT f FROM Foto f")
public class Foto implements Comparable<Foto>{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column 
	private String name;
	@Column (unique = true, nullable = false)
	private String path;
	@Column 
	private int click;
	
	
	//@Column (nullable = false)
	@ManyToOne
	private Album album;
//COSTRUTTORI
	public Foto(String name, String path, int click, Album album) {
		super();
		this.name = name;
		this.path = path;
		this.click = click;
		this.album = album;
	}
	public Foto() {
		this.click = 0;
	}
//METODI
	
	
	



	//GET & SET
	
	public Long getId() {
		return id;
	}
	
	public String getPath() {
		return path;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setClick(int click) {
		this.click = click;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getClick() {
		return click;
	}
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}
	
	//CompareTo in ordine di click e in caso di parità in ordine alfabetico (se le foto hanno lo stesso nome si ordinano per id)
	@Override
	public int compareTo(Foto o) {
		if (this.click == o.getClick()) 
			if(this.name.equalsIgnoreCase(o.getName())) return (int)(this.id- o.getId());
			else return this.name.compareTo(o.getName());
		else return this.click - o.getClick();
	}
	
	// EQUALS & HASHCODE
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Foto other = (Foto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	




	
	
	
}
