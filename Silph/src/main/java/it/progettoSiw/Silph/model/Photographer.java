package it.progettoSiw.Silph.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="photographers")
@NamedQuery(name= "findAllPhotographers", query= "SELECT p FROM Photographer p")
public class Photographer {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String cognome;
//	@Column
//	private Date dataNascita;
//	@Column
//	private String luogoNascita;
	@Column (length= 2000)
	private String descrizione;
	@Column (nullable = false)
	private String email;
	@Column (unique = true)
	private String avatarPath;
	// Social http
	
	@OneToMany (mappedBy = "photographer")
	private List<Album> albums; 
	
//COSTRUTTORI
	public Photographer(String name, String cognome, String avatarPath) {
		super();
		this.name = name;
		this.cognome = cognome;
		this.avatarPath = avatarPath;
		this.email = "a@e.com";
		this.descrizione = "Faucibus parturient mus phasellus vestibulum suspendisse dui vel ridiculus nibh diam placerat tellus scelerisque facilisi mus vestibulum arcu mus praesent in blandit. Conubia ullamcorper cum rhoncus vitae dapibus venenatis integer in donec egestas lacus nibh vestibulum habitasse accumsan parturient malesuada sociis auctor scelerisque vehicula urna eu proin euismod. Id facilisi suspendisse parturient leo mus condimentum natoque scelerisque ullamcorper odio tristique ultricies arcu ac condimentum facilisi scelerisque class commodo. Scelerisque sagittis magna mi duis iaculis id erat pharetra vestibulum condimentum hac suspendisse tempor leo aliquet penatibus parturient donec parturient parturient. Vehicula suspendisse sem a adipiscing est ad donec ultricies senectus magnis convallis a fringilla adipiscing vulputate dui elementum diam ipsum eleifend condimentum placerat facilisi viverra mollis scelerisque. Commodo cum vestibulum hendrerit sit condimentum at rutrum vulputate scelerisque erat convallis himenaeos consequat a hac ultrices nam vel suspendisse nascetur dictum vulputate sed at.";
	}
	public Photographer() {}
// METODI
	public List<Album> getOrderedByClickAlbums() {
		return sortByClick(albums);
	}

	public List<Album> sortByClick(List<Album> albums){
		List <Album> albumsOrdinati  = new ArrayList<Album> (albums);
		Collections.sort(albumsOrdinati);
		return albumsOrdinati;
	}
	
	public String getNomeIntero() {
		return name+" "+cognome;
	}
	//GET & SET Classici
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
//	public Date getDataNascita() {
//		return dataNascita;
//	}
//	public void setDataNascita(Date dataNascita) {
//		this.dataNascita = dataNascita;
//	}
//	public String getLuogoNascita() {
//		return luogoNascita;
//	}
//	public void setLuogoNascita(String luogoNascita) {
//		this.luogoNascita = luogoNascita;
//	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAvatarPath() {
		return avatarPath;
	}
	public void setAvatarPath(String avatarPath) {
		this.avatarPath = avatarPath;
	}
	public List<Album> getAlbums() {
		return albums;
	}
	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Photographer other = (Photographer) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

}
